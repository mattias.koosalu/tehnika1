package addnumbersinarray;

public class AddNumbersInArray {
	public static int addEvenNumbersInArray(int[] array) {
		int sum = 0;
		
		for (int number : array) {
			if (number <= 0 || number % 2 == 1) {
				continue;
			}
			sum += number;
		}
		
		return sum;
	}
}
