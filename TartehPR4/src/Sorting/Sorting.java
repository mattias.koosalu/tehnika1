package Sorting;

import java.util.Arrays;

public class Sorting {
	public static int[] sort(int[] array) {
		Arrays.sort(array);
		return array;
	}
}
