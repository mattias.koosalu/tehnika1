package sortingtest;

import static org.junit.Assert.*;

import java.util.Arrays;

import Sorting.Sorting;

import org.junit.Test;

public class SortingTest {

	@Test
	public void sortedEmptyArrayIsEmptyArray() {
		assertEquals("Array was not empty", Arrays.equals(Sorting.sort(new int[] {}), new int[] {}), true);
	}

	@Test
	public void sortedArrayWithOneElement() {
		assertEquals("Array was not equal to itself",
				Arrays.equals(Sorting.sort(new int[] {1}), new int[] {1}), true);
	}
	
	@Test
	public void unsortedArrayWithTwoElements() {
		assertEquals("Array was not sorted correctly",
				Arrays.equals(Sorting.sort(new int[] {2, 1}), new int[] {1, 2}), true);
	}
	
	@Test
	public void sortedArrayWithTwoElements() {
		assertEquals("Array was not sorted correctly",
				Arrays.equals(Sorting.sort(new int[] {1, 2}), new int[] {1, 2}), true);
	}
	
	@Test
	public void unsortedArrayWithThreeElements() {
		assertEquals("Array was not sorted correctly",
				Arrays.equals(Sorting.sort(new int[] {1, 3, 2}), new int[] {1, 2,3 }), true);
	}
	
	@Test
	public void sortedArrayWithThreeElements() {
		assertEquals("Array was not sorted correctly",
				Arrays.equals(Sorting.sort(new int[] {1, 2, 3}), new int[] {1, 2, 3}), true);
	}
	
	@Test
	public void sortedArrayWithTenElements() {
		assertEquals("Array was not sorted correctly",
				Arrays.equals(Sorting.sort(new int[] {1, 2, 3, 1, 4, 3, 7, 3, 120, 78}),
						new int[] {1, 1, 2, 3, 3, 3, 4, 7, 78, 120}), true);
	}
}
