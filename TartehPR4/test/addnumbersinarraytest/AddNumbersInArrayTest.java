package addnumbersinarraytest;

import static org.junit.Assert.*;

import addnumbersinarray.AddNumbersInArray;

import org.junit.Test;

public class AddNumbersInArrayTest {

	@Test
	public void arrayWithOneEvenNumber() {
		assertEquals("Did not return the only positive even number in the array",
				AddNumbersInArray.addEvenNumbersInArray(new int[] {2}), 2);
	}
	
	@Test
	public void returnsSumOfEvenNumbersInArray() {
		assertEquals("Did not return the sum of positive even numbers in the array",
				AddNumbersInArray.addEvenNumbersInArray(new int[] {2, 4, 6}), 12);
	}

	@Test
	public void negativeNumbersAreIgnored() {
		assertEquals("Did not return the sum of positive even numbers in the array", 
				AddNumbersInArray.addEvenNumbersInArray(new int[] {-2, -4, 2, 4, 6}), 12);	
	}
	
	@Test
	public void oddNumbersAreIgnored() {
		assertEquals("Did not return the sum of positive even numbers in the array", 
				AddNumbersInArray.addEvenNumbersInArray(new int[] {1, 2, 4, 6}), 12);
	}
	
	@Test
	public void negativeAndOddNumbersAreIgnored() {
		assertEquals("Did not return the sum of positive even numbers in the array", 
				AddNumbersInArray.addEvenNumbersInArray(new int[] {-2, -4, 1, 2, 4, 6}), 12);
	}
}
